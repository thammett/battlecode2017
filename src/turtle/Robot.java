package turtle;

import java.util.Objects;
import java.util.function.Predicate;

import shared.Vector;
import battlecode.common.BodyInfo;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public abstract class Robot {
	
	protected final RobotController controller;
	
	protected float bullets;
	protected float victoryPointCost;
	protected Vector here;
	
	public Robot(RobotController controller) {
		this.controller = Objects.requireNonNull(controller);
	}
	
	public void init() {
		// No-op
	}
	
	public abstract void turn() throws GameActionException;
	
	public void run() {
		init();
		while (true) {
			int round = controller.getRoundNum();
			try {
				bullets = controller.getTeamBullets();
				victoryPointCost = controller.getVictoryPointCost();
				if (bullets / victoryPointCost >= GameConstants.VICTORY_POINTS_TO_WIN - controller.getTeamVictoryPoints()
						|| round >= controller.getRoundLimit() - 1) {
					controller.donate(bullets);
				}
				here = Vector.position(controller.getLocation());
				turn();
			} catch (Exception e) {
				System.out.println("uncaught exception!");
				e.printStackTrace(System.out);
			}
			if (round != controller.getRoundNum()) {
				System.out.println("Passed bytecode limit in round " + round);
			}
			Clock.yield();
		}
	}
	
	/**
	 * Given a course, move to the closest available point within stride radius
	 * @param course the vector to add to the current position
	 * @throws GameActionException
	 * @return the new position after moving
	 */
	protected Vector move(Vector course) throws GameActionException {
		if (controller.hasMoved()) {
			System.out.println("already moved");
			return Vector.position(controller.getLocation());
		}
		
		Vector target = course.truncate(RobotPlayer.stride).add(here);// moved to within stride
		MapLocation targetLocation = target.toLocation();
		
		controller.setIndicatorDot(here.toLocation(), 47, 47, 47);
		controller.setIndicatorDot(targetLocation, 0, 0, 255);
		
		float senseRadius = RobotPlayer.stride + RobotPlayer.radius;
		BodyInfo collision = findCollision(target, controller.senseNearbyRobots(targetLocation, senseRadius, null));
		if (collision == null && RobotPlayer.type != RobotType.SCOUT) {
			collision = findCollision(target, controller.senseNearbyTrees(targetLocation, senseRadius, null));
		}
		
		if (collision == null) {
			if (doMove(targetLocation)) {
				return target;
			} else {
				return here;
			}
		} else if (course.magnitude() > RobotPlayer.stride) {
			Vector hereToTree = Vector.subtract(collision.getLocation(), here);
			double angle = hereToTree.angle();
			controller.setIndicatorLine(collision.getLocation(), here.toLocation(), 147, 47, 47);
			controller.setIndicatorLine(here.toLocation(), target.toLocation(), 47, 147, 47);
			
			double offsetAngle = getOffsetAngle(collision.getRadius(), hereToTree);
			
			if (course.rightOf(hereToTree)) {
				offsetAngle *= -1;
			}
			
			Vector offsetTarget = Vector.polar(RobotPlayer.stride, angle + offsetAngle).add(here);
			if (doMove(offsetTarget.toLocation())) {
				return offsetTarget;
			} else {
				offsetAngle *= -1;// try the other side of the circle
				offsetTarget = Vector.polar(RobotPlayer.stride, angle + offsetAngle).add(here);
				if (doMove(offsetTarget.toLocation())) {
					return offsetTarget;
				} else {
					System.out.println("stuck!");// Here's where we wish we had pathfinding
					return here;
				}
			}
		} else {
			float distance = collision.getRadius() + RobotPlayer.radius + 0.01f;
			if (doMove((target = target.subtract(collision.getLocation())//from obstacle to target, note target is reassigned
							.normalize()
							.scale(distance)
							.add(collision.getLocation())).toLocation())
					||doMove((target = here.subtract(collision.getLocation())// from obstacle to here, note target is reassigned
							.normalize()
							.scale(distance)
							.add(collision.getLocation())).toLocation())) {
				return target;
			} else {
				System.out.println("stuck!");
				return here;
			}
		}
	}
	
	private double getOffsetAngle(float obstacleRadius, Vector hereToObstacle) {
		double a = obstacleRadius + RobotPlayer.radius + 0.01;// fudge by 0.01
		double b = RobotPlayer.stride;
		double c = hereToObstacle.magnitude();
		return Math.acos(((b * b) + (c * c) - (a * a)) / (2 * b * c));
	}
	
	/**
	 * @return whether we moved
	 */
	private boolean doMove(MapLocation location) throws GameActionException {
		controller.setIndicatorDot(location, 0, 255, 0);
		if (controller.canMove(location)) {
			controller.move(location);
			return true;
		} else {
			System.out.println("couldn't move to " + location);
			return false;
		}
	}
	
	private BodyInfo findCollision(Vector target, BodyInfo[] obstacles) {
		int pre = Clock.getBytecodeNum();
		try {
			for (BodyInfo obstacle : obstacles) {
				Vector difference = target.subtract(obstacle.getLocation());// vector from center of obstacle to intendedPosition
				float radii = obstacle.getRadius() + RobotPlayer.radius;
				if (difference.magnitude() < radii) {// there's a collision when that vector isn't long enough for both radii to fit along it
					controller.setIndicatorDot(obstacle.getLocation(), 255, 0, 0);
					return obstacle;
				}
			}
			return null;
		} finally {
			System.out.println("findCollision took " + (Clock.getBytecodeNum() - pre));
		}
	}
	
	protected Direction findBuildLocation(float bodyRadius, Predicate<? super Direction> check) {
		MapLocation location = controller.getLocation();
		float senseRadius = RobotPlayer.radius + bodyRadius;
		
		Direction d = null;
		
		for (RobotInfo ri : controller.senseNearbyRobots(senseRadius)) {
			d = findBuildLocationsAdjacent(location, ri, check);
			if (d != null) {
				return d;
			}
		}
		
		for (TreeInfo ti : controller.senseNearbyTrees(senseRadius)) {
			d = findBuildLocationsAdjacent(location, ti, check);
			if (d != null) {
				return d;
			}
		}
		
		d = Direction.NORTH;
		int i = 12;
		while (--i > 0) {
			if (check.test(d)) {
				return d;
			}
			d = d.rotateLeftDegrees(30);
		}
		
		return null;
	}
	
	private Direction findBuildLocationsAdjacent(MapLocation currentLocation, BodyInfo obstacle,
			Predicate<? super Direction> check) {
		Vector hereToObstacle = Vector.subtract(obstacle.getLocation(), currentLocation);
		float base = hereToObstacle.angle();
		float offset = (float) getOffsetAngle(obstacle.getRadius(), hereToObstacle);
		
		if (offset != offset) {// true if offset is NaN
			offset = 0;
		}
		
		Direction direction = new Direction(base + offset);
		if (check.test(direction)) {
			return direction;
		} else {
			offset *= -1;
			direction = new Direction(base + offset);
			if (check.test(direction)) {
				return direction;
			}
		}
		return null;
	}
	
}
