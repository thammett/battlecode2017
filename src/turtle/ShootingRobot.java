package turtle;

import shared.KnownEnemyLocations;
import shared.Steering;
import shared.Vector;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public abstract class ShootingRobot extends Robot {

	public ShootingRobot(RobotController controller) {
		super(controller);
	}
	
	protected void doCombat(RobotInfo[] enemies, RobotInfo[] friends) throws GameActionException {
		if (!controller.hasMoved() && enemies.length > 0) {
			Vector course = Steering.separation(controller, 3);
			if (friends.length > enemies.length) {
				course = course.add(Vector.subtract(enemies[0].location, here).truncate(RobotPlayer.stride));
			} else {
				Vector enemyCenter = Vector.NULL;
				for (RobotInfo enemy : enemies) {
					enemyCenter = enemyCenter.add(enemy.location);
				}
				enemyCenter.scale(1f / enemies.length);
				course = course.add(here.subtract(enemyCenter));
			}
			here = move(course.normalize().scale(RobotPlayer.stride));
		}
		
		if (enemies.length > 0 && controller.canFireSingleShot()) {
			controller.fireSingleShot(Vector.subtract(enemies[0].location, here).toDirection());
		}
		
		if (!controller.hasMoved()) {
			Vector enemy = KnownEnemyLocations.getNearbyKnownEnemyLocation(controller, here);
			move(enemy.subtract(here)
					.add(Steering.separation(controller, 5))
					.add(Steering.wallAvoidance(controller))
					.normalize()
					.scale(RobotPlayer.stride));
		}
	}
	
}
