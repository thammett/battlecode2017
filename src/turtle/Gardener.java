package turtle;

import shared.BuildOrder;
import shared.HexGrid;
import shared.Steering;
import shared.TreeCuttingAssignments;
import shared.Vector;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.TreeInfo;

public strictfp class Gardener extends Robot {
	
	private static final float BULLET_STOCKPILE = 700f;
	private static final float SEPARATION_RANGE = 5;
	
	public Gardener(RobotController rc) {
		super(rc);
	}
	
	@Override
	public void turn() throws GameActionException {
		
		Vector[] gridPoints = HexGrid.computeNearestGridPoints(controller, here);
		boolean treesPlanted = false;
		
		if (!BuildOrder.finished(controller)) {
			if (build(BuildOrder.getNextType(controller))) {
				BuildOrder.increment(controller);
			}
		} else {
			treesPlanted = true;
			for (Vector point : gridPoints) {
				treesPlanted &= farm(point);
			}
		}
		
		if (!controller.hasMoved()) {
			move(Steering.separation(controller, SEPARATION_RANGE)
					.add(Steering.wallAvoidance(controller))
					.normalize().scale(RobotPlayer.stride));
		}
		
		if (treesPlanted && controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team).length < 7) {
			if (enemyTreeHealth() > 500) {
				build(RobotType.LUMBERJACK);
			} else if (controller.getRoundNum() < 500) {
				build(RobotType.SCOUT);
			} else {
				build(RobotType.SOLDIER);
			}
		}
		
		if (bullets > BULLET_STOCKPILE) {
			double donation = Math.floor((bullets - BULLET_STOCKPILE) / victoryPointCost) * victoryPointCost;
			if (donation > 0) {
				controller.donate((float) donation);
			}
		}
	}
	
	private float enemyTreeHealth() {
		float health = 0;
		for (TreeInfo tree : controller.senseNearbyTrees()) {
			if (tree.team != RobotPlayer.team) {
				health += tree.health;
			}
		}
		return health;
	}
	
	private boolean farm(Vector hexLocation) throws GameActionException {
		Vector location = HexGrid.hexToPixel(hexLocation);
		TreeInfo existing = controller.senseTreeAtLocation(location.toLocation());
		Vector difference = location.subtract(here);
		if (existing != null && existing.team == RobotPlayer.team) {
			float distance = existing.radius + GameConstants.INTERACTION_DIST_FROM_EDGE - 0.05f;
			if (controller.canWater() && existing.health < existing.maxHealth - GameConstants.WATER_HEALTH_REGEN_RATE) {
				if (difference.magnitude() <= RobotPlayer.radius + distance || maybeMoveAlong(difference, distance)) {
					System.out.println(String.format("Watering:%nlocation: %s%ntree: %s%ndistance: %f%n moved: %b",
							here, location, difference.magnitude(), controller.hasMoved()));
					controller.water(existing.ID);
				}
			}
			if (controller.canShake() && existing.containedBullets > 5) {
				if (difference.magnitude() <= RobotPlayer.radius + distance || maybeMoveAlong(difference, distance)) {
					System.out.println(String.format("Shaking:%nlocation: %s%ntree: %s%ndistance: %f%n moved: %b",
							here, location, difference.magnitude(), controller.hasMoved()));
					controller.shake(existing.ID);
				}
			}
			return true;
		} else if (existing != null && existing.team != RobotPlayer.team) {
			TreeCuttingAssignments.submitTree(controller, existing);
			return false;
		} else if (existing == null && controller.hasTreeBuildRequirements()) {
			if (!controller.isCircleOccupiedExceptByThisRobot(location.toLocation(), GameConstants.BULLET_TREE_RADIUS)
					&& maybeMoveAlong(difference, GameConstants.BULLET_TREE_RADIUS)) {
				Direction d = difference.toDirection();
				if (controller.canPlantTree(d)) {
					controller.plantTree(d);
					return true;
				}
			} else if (controller.hasMoved()) {
				System.out.println("would plant a tree but I've already moved");
			} else {
				TreeInfo[] trees = controller.senseNearbyTrees(location.toLocation(), GameConstants.BULLET_TREE_RADIUS, null);
				for (TreeInfo tree : trees) {
					if (tree.team != RobotPlayer.team) {
						TreeCuttingAssignments.submitTree(controller, tree);
					}
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	private boolean maybeMoveAlong(Vector difference, float within) throws GameActionException {
		Vector truncated = difference
				.truncate(difference.magnitude() - within - RobotPlayer.radius);
		MapLocation target = truncated.add(here).toLocation();
		if (!controller.hasMoved() && controller.canMove(target)) {
			System.out.println(String.format("moving from %s to %s", here, target));
			controller.move(target);
			if (truncated.magnitude() <= RobotPlayer.stride) {
				return true;
			}
		}
		return false;
	}
	
	private boolean build(RobotType toBuild) throws GameActionException {
		if (controller.hasRobotBuildRequirements(toBuild)) {
			Direction target = findBuildLocation(toBuild.bodyRadius, d -> controller.canBuildRobot(toBuild, d));
			if (target != null) {
				controller.buildRobot(toBuild, target);
				return true;
			}
		}
		return false;
	}
	
}
