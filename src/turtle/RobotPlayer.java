package turtle;

import shared.KnownEnemyLocations;
import shared.MapInfo;
import battlecode.common.Clock;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Team;

public strictfp class RobotPlayer {
	
	public static float radius;
	public static float stride;
	public static Team team;
	public static RobotType type;
	
	public static void run(RobotController rc) {
		type = rc.getType();
		radius = type.bodyRadius;
		stride = type.strideRadius;
		team = rc.getTeam();
		
		MapInfo.init(rc);
		KnownEnemyLocations.init(rc);
		
		switch (type) {
		case ARCHON:
			new Archon(rc).run();
			break;
		case GARDENER:
			new Gardener(rc).run();
			break;
		case SCOUT:
			new Scout(rc).run();
			break;
		case LUMBERJACK:
			new Lumberjack(rc).run();
			break;
		case SOLDIER:
		case TANK:
			new Soldier(rc).run();
		}
	}
	
}
