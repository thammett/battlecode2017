package turtle;

import shared.BuildOrder;
import shared.HexGrid;
import shared.Steering;
import shared.Vector;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public final class Archon extends Robot {
	
	private boolean builtFirstGardener;
	
	public Archon(RobotController controller) {
		super(controller);
	}
	
	@Override
	public void turn() throws GameActionException {
		Vector nearestGridPoint = HexGrid.hexToPixel(HexGrid.hexRound(HexGrid.pixelToHex(here)));
		Vector avoidance = Steering.wallAvoidance(controller).add(Steering.separation(controller, 3));
		move(avoidance.magnitude() < 0.125 ? avoidance.add(nearestGridPoint.subtract(here)) : avoidance);
		
		if ((BuildOrder.finished(controller) || !builtFirstGardener)
				&& nearbyGardeners() < 15
				&& controller.hasRobotBuildRequirements(RobotType.GARDENER)) {
			Direction target = findBuildLocation(RobotType.GARDENER.bodyRadius, d -> controller.canHireGardener(d));
			if (target != null) {
				controller.hireGardener(target);
				builtFirstGardener = true;
			}
		}
	}
	
	private int nearbyGardeners() {
		int count = 0;
		for (RobotInfo ri : controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team)) {
			if (ri.type == RobotType.GARDENER) {
				count ++;
			}
		}
		return count;
	}
	
}
