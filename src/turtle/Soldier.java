package turtle;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;


public class Soldier extends ShootingRobot {
	
	public Soldier(RobotController controller) {
		super(controller);
	}
	
	@Override
	public void turn() throws GameActionException {
		RobotInfo[] enemies = controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team.opponent());
		RobotInfo[] friends = controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team);
		doCombat(enemies, friends);
	}
	
}
