package turtle;

import shared.Vector;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.TreeInfo;

public final class Scout extends ShootingRobot {
	
	public Scout(RobotController controller) {
		super(controller);
	}
	
	@Override
	public void turn() throws GameActionException {
		// move towards known enemy locations
		// shake trees along the way
		// get as close as possible to gardeners, while dodging lumberjacks and bullets
		
		
		// priorities:
		// 1) combat
		// 		dodge bullets, snipe gardeners
		// 		if no gardeners retreat unless local advantage
		// 2) get bullets
		// 3) find enemy
		RobotInfo[] enemies = controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team.opponent());
		RobotInfo[] friends = controller.senseNearbyRobots(RobotPlayer.type.sensorRadius, RobotPlayer.team);
		
		attackGardener(enemies);
		pickUpBullets();
		doCombat(enemies, friends);
	}
	
	private void attackGardener(RobotInfo[] enemies) throws GameActionException {
		RobotInfo nearestGardener = null;
		for (RobotInfo ri : enemies) {
			if (ri.type == RobotType.GARDENER) {
				nearestGardener = ri;
				break;
			}
		}
		
		if (nearestGardener != null) {
			Vector target = Vector.position(nearestGardener.location);
			Vector offset = Vector.NULL;
			for (RobotInfo ri : enemies) {
				Vector away = target.subtract(ri.location);
				float distance = away.magnitude() - RobotPlayer.radius - ri.type.bodyRadius;
				offset = offset.add(away.normalize().scale(1 / (distance * distance)));
			}
			
			here = move(target
					.add(offset.normalize().scale(nearestGardener == null ? RobotPlayer.stride : 0.25f))
					.subtract(here));
			Direction direction = target.subtract(here).toDirection();
			if (controller.canFireSingleShot() && direction != null) {
				controller.fireSingleShot(direction);
			}
		}
	}
	
	private void pickUpBullets() throws GameActionException {
		float range = RobotPlayer.radius + GameConstants.INTERACTION_DIST_FROM_EDGE;
		TreeInfo best = findMostBulletsWithin(RobotPlayer.type.sensorRadius);
		if (best != null && best.getContainedBullets() > 0) {
			if (here.subtract(best.location).magnitude() < range) {
				shake(best.ID);
			} else {
				here = move(Vector.subtract(best.location, here));
				TreeInfo bestWithoutMoving = findMostBulletsWithin(range);
				if (bestWithoutMoving != null) {
					shake(bestWithoutMoving.ID);
				}
			}
		}
	}
	
	private TreeInfo findMostBulletsWithin(float range) {
		TreeInfo best = null;
		for (TreeInfo tree : controller.senseNearbyTrees(range, Team.NEUTRAL)) {
			if (best == null || tree.getContainedBullets() > best.getContainedBullets()) {
				best = tree;
			}
		}
		return best;
	}
	
	private void shake(int treeID) throws GameActionException {
		if (controller.canShake(treeID)) {
			controller.shake(treeID);
		} else {
			System.out.println("unexpectedly can't shake tree " + treeID);
		}
	}
	
}
