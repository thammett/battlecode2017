package turtle;

import shared.Steering;
import shared.TreeCuttingAssignments;
import shared.Vector;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import battlecode.common.TreeInfo;


public class Lumberjack extends Robot {
	
	public Lumberjack(RobotController controller) {
		super(controller);
	}
	
	@Override
	public void turn() throws GameActionException {
		Vector toCut = TreeCuttingAssignments.getAssignment(controller, here);
		System.out.println("My assignment: " + toCut);
		if (toCut != null) {
			controller.setIndicatorDot(toCut.toLocation(), 255, 255, 255);
			if (toCut.subtract(here).magnitude() >= RobotPlayer.radius + GameConstants.INTERACTION_DIST_FROM_EDGE) {
				here = move(toCut.subtract(here));
			}
			if (toCut.subtract(here).magnitude() < RobotPlayer.type.sensorRadius) {
				TreeInfo tree = controller.senseTreeAtLocation(toCut.toLocation());
				if (tree != null) {
					if (controller.canChop(toCut.toLocation())) {
						float health = tree.getHealth();
						System.out.println("chopping tree " + tree);
						controller.chop(toCut.toLocation());
						if (health <= GameConstants.LUMBERJACK_CHOP_DAMAGE) {
							TreeCuttingAssignments.completeAssignment(controller);
						}
					}
				} else {
					TreeCuttingAssignments.completeAssignment(controller);
				}
			}
		} else {
			move(Steering.separation(controller, 5)
					.add(Steering.wallAvoidance(controller))
					.normalize()
					.scale(RobotPlayer.stride));
		}
		
		System.out.println("trying to chop nearby trees");
		for (TreeInfo ti : controller.senseNearbyTrees(RobotPlayer.radius + GameConstants.INTERACTION_DIST_FROM_EDGE)) {
			if (ti.team != RobotPlayer.team) {
				controller.chop(ti.ID);
			}
		}
	}
	
}
