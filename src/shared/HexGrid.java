package shared;

import battlecode.common.RobotController;

public class HexGrid {
	
	private static final double ROOT_3 = Math.sqrt(3);
	private static final double TREE_GRID_SIZE = 4.1 / ROOT_3;
	
	public static Vector[] computeNearestGridPoints(RobotController rc, Vector location) {
		Vector axial = pixelToHex(location);
		
		float dz = -axial.dx - axial.dy;
		int rx = Math.round(axial.dx);
		int ry = Math.round(axial.dy);
		int rz = Math.round(dz);
		
		if (rx + ry + rz == 0) {
			float xDiff = Math.abs(axial.dx - rx);
			float yDiff = Math.abs(axial.dy - ry);
			float zDiff = Math.abs(dz - rz);
			
			if (xDiff > yDiff && xDiff > zDiff) {
				rx += Math.signum(axial.dx - rx);
			} else if (yDiff > zDiff) {
				ry += Math.signum(axial.dy - ry);
			} else {
				rz += Math.signum(dz - rz);
			}
		}
		
		Vector xy = new Vector(rx, ry);
		Vector xz = new Vector(rx, -rx - rz);
		Vector yz = new Vector(-ry - rz, ry);
		
		rc.setIndicatorDot(hexToPixel(xy).toLocation(), 255, 255, 0);
		rc.setIndicatorDot(hexToPixel(xz).toLocation(), 255, 255, 0);
		rc.setIndicatorDot(hexToPixel(yz).toLocation(), 255, 255, 0);
		return new Vector[]{xy, xz, yz};
	}
	
	public static Vector pixelToHex(Vector location) {
		double q = location.dx * 2 / 3 / TREE_GRID_SIZE;
		double r = (-location.dx / 3 + ROOT_3/3 * location.dy) / TREE_GRID_SIZE;
		return new Vector(q, r);
	}
	
	public static Vector hexToPixel(Vector axial) {
		double x = TREE_GRID_SIZE * 3/2 * axial.dx;
		double y = TREE_GRID_SIZE * ROOT_3 * (axial.dy + axial.dx/2);
		return new Vector(x, y);
	}
	
	public static Vector hexRound(Vector hexLocation) {
		float dz = -hexLocation.dx - hexLocation.dy;
		int rx = Math.round(hexLocation.dx);
		int ry = Math.round(hexLocation.dy);
		int rz = Math.round(dz);
		
		float xDiff = Math.abs(hexLocation.dx - rx);
		float yDiff = Math.abs(hexLocation.dy - ry);
		float zDiff = Math.abs(dz - rz);
		
		if (xDiff > yDiff && xDiff > zDiff) {
			rx = -ry - rz;
		} else if (yDiff > zDiff) {
			ry = -rx - rz;
		}
		
		return new Vector(rx, ry);
	}
	
}
