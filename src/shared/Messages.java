package shared;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public final class Messages {
	
	private static final int TOP_INDEX = 0;
	private static final int LEFT_INDEX = 1;
	private static final int RIGHT_INDEX = 2;
	private static final int BOTTOM_INDEX = 3;
	
	private static final int BUILD_ORDER_COUNT_INDEX = 4;
	
	private static final int TREE_ASSIGNMENTS_INSERT = 5;// the value of the int at this channel is the first invalid index
	private static final int TREE_ASSIGNMENTS_FINISHED = 6;// start reading at the value of the int at this channel
	private static final int TREE_ASSIGNMENTS_FIRST_TREE = 7;
	private static final int CHANNELS_PER_TREE = 3;// x, y, deadline
	private static final int TREE_ASSIGNMENTS_SIZE = 100;
	
	private static int treeAccess = -1;
	private static int cachedInsert = 0;
	private static int cachedFinished = 0;
	private static float[] cachedTreesX = new float[TREE_ASSIGNMENTS_SIZE];
	private static float[] cachedTreesY = new float[TREE_ASSIGNMENTS_SIZE];
	private static int[] cachedTreesDeadline = new int[TREE_ASSIGNMENTS_SIZE];
	
	public static final int KNOWN_ENEMY_LOCATIONS_START = TREE_ASSIGNMENTS_FIRST_TREE + CHANNELS_PER_TREE * TREE_ASSIGNMENTS_SIZE;
	public static final int KNOWN_ENEMY_LOCATIONS_DIMENSION = 26;// number  of 8 bit pages across: ceil(201/8.0) == 26
	public static final int KNOWN_ENEMY_LOCATIONS_LENGTH = KNOWN_ENEMY_LOCATIONS_DIMENSION * KNOWN_ENEMY_LOCATIONS_DIMENSION * 2;// takes two ints to store each page
	
	public static float readMapTop(RobotController rc) {
		return readFloat(rc, TOP_INDEX);
	}
	
	private static float readFloat(RobotController rc, int channel) {
		try {
			return rc.readBroadcastFloat(channel);
		} catch (GameActionException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static void writeMapTop(RobotController rc, float top) {
		writeFloat(rc, TOP_INDEX, top);
	}
	
	private static void writeFloat(RobotController rc, int channel, float value) {
		try {
			rc.broadcast(channel, Float.floatToRawIntBits(value));
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public static float readMapLeft(RobotController rc) {
		return readFloat(rc, LEFT_INDEX);
	}
	
	public static void writeMapLeft(RobotController rc, float left) {
		writeFloat(rc, LEFT_INDEX, left);
	}
	
	public static float readMapRight(RobotController rc) {
		return readFloat(rc, RIGHT_INDEX);
	}
	
	public static void writeMapRight(RobotController rc, float right) {
		writeFloat(rc, RIGHT_INDEX, right);
	}
	
	public static float readMapBottom(RobotController rc) {
		return readFloat(rc, BOTTOM_INDEX);
	}
	
	public static void writeMapBottom(RobotController rc, float bottom) {
		writeFloat(rc, BOTTOM_INDEX, bottom);
	}
	
	public static int readBuildCount(RobotController rc) {
		return readInt(rc, BUILD_ORDER_COUNT_INDEX);
	}
	
	public static int readInt(RobotController rc, int channel) {
		try {
			return rc.readBroadcast(channel);
		} catch (GameActionException e) {
			System.out.println("invalid channel " + channel);
			e.printStackTrace();
			return 0;
		}
	}
	
	public static void writeBuildCount(RobotController rc, int count) {
		writeInt(rc, BUILD_ORDER_COUNT_INDEX, count);
	}
	
	public static void writeInt(RobotController rc, int channel, int value) {
		try {
			rc.broadcast(channel, value);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeTree(RobotController rc, float x, float y, int deadline) {
		readAllTrees(rc);
		if ((cachedInsert + 1) % TREE_ASSIGNMENTS_SIZE == cachedFinished || findTree(x, y) != -1) {
			return;
		}
		
		int channel = getChannelForTree(cachedInsert);
		writeFloat(rc, channel, x);
		writeFloat(rc, channel + 1, y);
		writeInt(rc, channel + 2, deadline);
		writeInt(rc, TREE_ASSIGNMENTS_INSERT, cachedInsert + 1);
	}
	
	public static int getDeadline(RobotController rc, Vector tree) {
		readAllTrees(rc);
		int index = findTree(tree.dx, tree.dy);
		if (index == -1) {
			return -1;
		} else {
			return cachedTreesDeadline[index];
		}
	}
	
	private static int findTree(float x, float y) {
		System.out.println(String.format("looking for %f, %f", x, y));
		for (int i = cachedFinished; i != cachedInsert; i = (i + 1) % TREE_ASSIGNMENTS_SIZE) {
			System.out.println(String.format("checking index %d (%f, %f)", i, cachedTreesX[i], cachedTreesY[i]));
			if (cachedTreesX[i] == x && cachedTreesY[i] == y) {
				System.out.println("found a match");
				return i;
			}
		}
		return -1;
	}
	
	private static void readAllTrees(RobotController rc) {
		if (rc.getRoundNum() == treeAccess) {
			return;
		}
		cachedInsert = readInt(rc, TREE_ASSIGNMENTS_INSERT);
		cachedFinished = readInt(rc, TREE_ASSIGNMENTS_FINISHED);
		for (int i = cachedFinished; i != cachedInsert; i = (i + 1) % TREE_ASSIGNMENTS_SIZE) {
			int channel = getChannelForTree(i);
			cachedTreesX[i] = readFloat(rc, channel);
			cachedTreesY[i] = readFloat(rc, channel + 1);
			cachedTreesDeadline[i] = readInt(rc, channel + 2);
		}
		System.out.println("Trees in list: " + (cachedFinished <= cachedInsert ? cachedInsert - cachedFinished : TREE_ASSIGNMENTS_SIZE + cachedInsert - cachedFinished));
	}
	
	public static Vector findNearestTree(RobotController rc, Vector nearest) {
		readAllTrees(rc);
		float leastDistance = Float.MAX_VALUE;
		int bestIndex = -1;
		
		for (int i = cachedFinished; i != cachedInsert; i = (i + 1) % TREE_ASSIGNMENTS_SIZE) {
			if (cachedTreesDeadline[i] > rc.getRoundNum()) {
				continue;
			}
			
			float distance = nearest.subtract(new Vector(cachedTreesX[i], cachedTreesY[i])).magnitude();
			if (distance < leastDistance) {
				leastDistance = distance;
				bestIndex = i;
			}
		}
		
		return bestIndex == -1 ? null : new Vector(cachedTreesX[bestIndex], cachedTreesY[bestIndex]);
	}
	
	public static void bidOnTreeAt(RobotController rc, Vector tree, int bid) {
		readAllTrees(rc);
		for (int i = cachedFinished; i != cachedInsert; i = (i + 1) % TREE_ASSIGNMENTS_SIZE) {
			if (cachedTreesX[i] == tree.dx && cachedTreesY[i] == tree.dy) {
				int cachedDeadline = cachedTreesDeadline[i];
				if (cachedDeadline < rc.getRoundNum() || bid < cachedDeadline) {
					writeInt(rc, getChannelForTree(i) + 2, bid);
				}
			}
		}
	}
	
	private static int getChannelForTree(int i) {
		return TREE_ASSIGNMENTS_FIRST_TREE + i * CHANNELS_PER_TREE;
	}
	
	public static void removeTree(RobotController rc, Vector tree) {
		readAllTrees(rc);
		int index = findTree(tree.dx, tree.dy);
		if (index != -1) {
			if (index != cachedFinished) {
				int channel = getChannelForTree(index);
				writeFloat(rc, channel, cachedTreesX[cachedFinished]);
				writeFloat(rc, channel + 1, cachedTreesY[cachedFinished]);
				writeInt(rc, channel + 2, cachedTreesDeadline[cachedFinished]);
			}
			writeInt(rc, TREE_ASSIGNMENTS_FINISHED, (cachedFinished + 1) % TREE_ASSIGNMENTS_SIZE);
		}
	}
	
}
