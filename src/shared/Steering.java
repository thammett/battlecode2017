package shared;

import turtle.RobotPlayer;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public final class Steering {
	
	private static final float WANDER_CHANGE_RANGE = (float) (Math.PI / 16);
	private static final float WANDER_CHANGE_OFFSET = WANDER_CHANGE_RANGE / 2;
	private static final float WANDER_LEAD = 2f;
	private static final float WANDER_CIRCLE_RADIUS = 0.25f;
	
	private static final float WALL_AVOIDANCE_RANGE = 5;
	
	private static float wanderAngle;
	
	private static int turn = 0;
	private static MapLocation previousLocation;
	private static MapLocation currentLocation;
	private static Vector velocity;
	
	public static Vector separation(RobotController rc, float radius) {
		initSteeringStatics(rc);
		Vector v = new Vector(0, 0);
		for (RobotInfo ri : rc.senseNearbyRobots(radius)) {
			MapLocation here = rc.getLocation();
			Vector away = Vector.subtract(here, ri.location);
			rc.setIndicatorLine(currentLocation, away.scale(-1).add(currentLocation).toLocation(), 0, 0, 0);
			float distance = away.magnitude() - RobotPlayer.radius - ri.type.bodyRadius;
			v = v.add(away.normalize().scale(1 / (distance * distance)));
		}
		rc.setIndicatorLine(currentLocation, v.add(currentLocation).toLocation(), 255, 255, 255);
		System.out.println("Separation: " + v);
		return v;
	}
	
	public static Vector wander(RobotController rc) {
		initSteeringStatics(rc);
		Vector circleCenter = velocity.normalize().scale(WANDER_LEAD * RobotPlayer.stride);
		
		wanderAngle += Math.random() * WANDER_CHANGE_RANGE - WANDER_CHANGE_OFFSET;
		Vector result = circleCenter
				.add(Vector.unit(wanderAngle + velocity.angle()).scale(WANDER_CIRCLE_RADIUS))
				.truncate(RobotPlayer.stride);
		System.out.println("Wander: " + result);
		return result;
	}
	
	private static void initSteeringStatics(RobotController rc) {
		if (turn == rc.getRoundNum()) {
			return;
		}
		
		MapLocation here = rc.getLocation();
		if (currentLocation != null) {
			previousLocation = currentLocation;
		} else {
			previousLocation = new MapLocation(here.x, here.y - 0.01f);
		}
		currentLocation = here;
		velocity = Vector.subtract(currentLocation, previousLocation);
		turn = rc.getRoundNum();
	}
	
	public static Vector wallAvoidance(RobotController rc) {
		initSteeringStatics(rc);
		MapInfo.checkForBoundaries(rc, currentLocation);
		
		Vector avoidance = new Vector(0, 0);
		if (MapInfo.right > 0 && currentLocation.x >= MapInfo.right - RobotPlayer.radius - WALL_AVOIDANCE_RANGE) {
			float distance = MapInfo.right - RobotPlayer.radius - currentLocation.x;
			avoidance = avoidance.add(Vector.unit(Math.PI).scale(1 / (distance * distance)));
		}
		if (MapInfo.left > 0 && currentLocation.x <= MapInfo.left + RobotPlayer.radius + WALL_AVOIDANCE_RANGE) {
			float distance = MapInfo.left + RobotPlayer.radius - currentLocation.x;// negative, but will be squared
			avoidance = avoidance.add(Vector.unit(0).scale(1 / (distance * distance)));
		}
		if (MapInfo.top > 0 && currentLocation.y >= MapInfo.top - RobotPlayer.radius - WALL_AVOIDANCE_RANGE) {
			float distance = MapInfo.top - RobotPlayer.radius - currentLocation.y;
			avoidance = avoidance.add(Vector.unit(Math.PI / -2).scale(1 / (distance * distance)));
		}
		if (MapInfo.bottom > 0 && currentLocation.y <= MapInfo.bottom + RobotPlayer.radius + WALL_AVOIDANCE_RANGE) {
			float distance = MapInfo.bottom + RobotPlayer.radius - currentLocation.y;// negative, but will be squared
			avoidance = avoidance.add(Vector.unit(Math.PI / 2).scale(1 / (distance * distance)));
		}
		
		System.out.println("Wall avoidance: " + avoidance);
		
		return avoidance;
	}
	
}
