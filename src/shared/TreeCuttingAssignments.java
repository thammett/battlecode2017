package shared;

import turtle.RobotPlayer;
import battlecode.common.RobotController;
import battlecode.common.TreeInfo;

public class TreeCuttingAssignments {
	
	private static Vector assigned;
	private static int bid;
	
	
	public static void submitTree(RobotController rc, TreeInfo tree) {
		System.out.println("submitting tree " + tree);
		Messages.writeTree(rc, tree.location.x, tree.location.y, 0);
	}
	
	public static Vector getAssignment(RobotController rc, Vector currentLocation) {
		System.out.println("in getAssignement: bid is " + bid + " assignment is: " + assigned);
		if (bid > 0 && bid == Messages.getDeadline(rc, assigned)) {
			return assigned;
		} else {
			bid = 0;
			assigned = null;
			Vector nearest = Messages.findNearestTree(rc, currentLocation);
			if (nearest != null) {
				assigned = nearest;
				bid = (int) Math.ceil(nearest.subtract(currentLocation).magnitude() / RobotPlayer.stride)
						+ rc.getRoundNum();
				System.out.println("bidding on tree " + assigned + " bid: " + bid);
				Messages.bidOnTreeAt(rc, nearest, bid);
				return null;// next time, if no-one outbids us, it will be ours
			} else {
				return null;
			}
		}
		
		// check cached assignment
		// if we bid last turn, and we havent been outbid (that is, if the deadline is the same)
		// 		return the vector of that tree's position
		// if we bid last turn and we've been outbid, or if there's no cached assignment
		//		loop through the unassigned trees (those between end and start with a deadline in the past)
		// 		find the closest one to the current location, and bid on it (update its description)
	}
	
	public static void completeAssignment(RobotController rc) {
		// if our tree is NOT at logical index DELETE + 1, copy the one at delete + 1 to our logical index
		// increment the delete counter
		// clear cached assignment
		Messages.removeTree(rc, assigned);
		assigned = null;
		bid = 0;
	}
	
}
