package shared;

import turtle.RobotPlayer;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Team;

public class MapInfo {
	
	// TODO use broadcast sense to detect range
	public static float left;
	public static float top;
	public static float right;
	public static float bottom;
	
	public static boolean knowSymmetry;
	
	public static MapLocation rotationCenter;
	
	public static boolean verticalAxis;
	public static float centerLine;
	
	public static void init(RobotController rc) {
		//TODO check if it's cheaper to read these from broadcast array
		MapLocation[] teamALocations = rc.getInitialArchonLocations(Team.A);
		MapLocation[] teamBLocations = rc.getInitialArchonLocations(Team.B);
		
		MapLocation a0 = teamALocations[0];
		MapLocation b0 = teamBLocations[0];
		
		System.out.println(String.format("a0: %s, b0: %s", a0, b0));
		
		if (a0.x != b0.x && a0.y != b0.y) {
			computeRotationalSymmetry(a0, b0);
		} else if (teamALocations.length > 1) {
			MapLocation a1 = teamALocations[1];
			MapLocation b1 = teamBLocations[1];
			System.out.println(String.format("a1: %s, b1: %s", a1, b1));
			if (a1.x != b1.x && a1.y != b1.y) {
				computeRotationalSymmetry(a0, b0);
			} else {
				computeReflectiveSymmetry(a0, b0);
			}
		} else {
			// Don't know whether rotational or reflective
		}
		
		float readTop = Messages.readMapTop(rc);
		if (readTop > 0) {
			top = readTop;
		}
		
		float readLeft = Messages.readMapLeft(rc);
		if (readLeft > 0) {
			left = readLeft;
		}
		
		float readRight = Messages.readMapRight(rc);
		if (readRight > 0) {
			right = readRight;
		}
		
		float readBottom = Messages.readMapBottom(rc);
		if (readBottom > 0) {
			bottom = readBottom;
		}
	}
	
	private static void computeRotationalSymmetry(MapLocation a0, MapLocation b0) {
		rotationCenter = new MapLocation((a0.x + b0.x) / 2, (a0.y + b0.y) / 2);
		knowSymmetry = true;
	}
	
	private static void computeReflectiveSymmetry(MapLocation a0, MapLocation b0) {
		verticalAxis = a0.y == b0.y;
		centerLine = verticalAxis ? (a0.x + b0.x) / 2 : (a0.y + b0.y) / 2;
		knowSymmetry = true;
	}
	
	public static void checkForBoundaries(RobotController rc, MapLocation currentLocation) {
		try {
			if ((MapInfo.right <= 0 || MapInfo.left <= 0 || MapInfo.top <= 0 || MapInfo.bottom <= 0)
					&& !rc.onTheMap(currentLocation, RobotPlayer.type.sensorRadius)) {
				
				float offset = 0;
				float range = RobotPlayer.type.sensorRadius / 2;
				for (; range >= 0.0125; range /= 2) {
					if (rc.onTheMap(currentLocation, offset + range)) {
						offset += range;
					}
				}
				// now offset is a lower bound of the distance from here to the nearest edge of the map
				
				if (!rc.onTheMap(currentLocation.add(Direction.NORTH, offset), range * 2)) {
					MapInfo.setTop(rc, currentLocation.y + offset);
				} else if (!rc.onTheMap(currentLocation.add(Direction.SOUTH, offset), range * 2)) {
					MapInfo.setBottom(rc, currentLocation.y - RobotPlayer.radius - offset);
				} else if (!rc.onTheMap(currentLocation.add(Direction.EAST, offset), range * 2)) {
					MapInfo.setRight(rc, currentLocation.x + RobotPlayer.radius + offset);
				} else if (!rc.onTheMap(currentLocation.add(Direction.WEST, offset), range * 2)) {
					MapInfo.setLeft(rc, currentLocation.x - RobotPlayer.radius - offset);
				} else {
					System.out.println("Something went wrong checking map boundaries");
				}
			}
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public static void setTop(RobotController rc, float value) {
		top = value;
		Messages.writeMapTop(rc, value);
		if (bottom == 0) {
			if (rotationCenter != null) {
				setBottom(rc, 2 * rotationCenter.y - top);
			} else if (centerLine > 0 && !verticalAxis) {
				setBottom(rc, 2 * centerLine - top);
			}
		}
	}
	
	public static void setBottom(RobotController rc, float value) {
		bottom = value;
		Messages.writeMapBottom(rc, bottom);
		if (top == 0) {
			if (rotationCenter != null) {
				setTop(rc, 2 * rotationCenter.y - top);
			} else if (centerLine > 0 && !verticalAxis) {
				setTop(rc, 2 * centerLine - bottom);
			}
		}
	}
	
	public static void setLeft(RobotController rc, float value) {
		left = value;
		Messages.writeMapLeft(rc, value);
		if (right == 0) {
			if (rotationCenter != null) {
				setRight(rc, 2 * rotationCenter.x - left);
			} else if (centerLine > 0 && verticalAxis) {
				setRight(rc, 2 * centerLine - left);
			}
		}
	}
	
	public static void setRight(RobotController rc, float value) {
		right = value;
		Messages.writeMapRight(rc, right);
		if (left == 0) {
			if (rotationCenter != null) {
				setLeft(rc, 2 * rotationCenter.x - right);
			} else if (centerLine > 0 && verticalAxis) {
				setLeft(rc, 2 * centerLine - right);
			}
		}
	}
	
}
