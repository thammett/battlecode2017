package shared;

import turtle.RobotPlayer;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;


public final class KnownEnemyLocations {
	
	public static int minX = Integer.MIN_VALUE;
	public static int minY = Integer.MIN_VALUE;
	
	private static final int[] BIT_POSITION_LOOKUP = {
		32, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4,
		7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5,
		20, 8, 19, 18
	};
	
	public static void init(RobotController rc) {
		minX = (int) Math.floor(rc.getInitialArchonLocations(RobotPlayer.team)[0].x) - 100;
		minY = (int) Math.floor(rc.getInitialArchonLocations(RobotPlayer.team)[0].y) - 100;
		for (MapLocation location : rc.getInitialArchonLocations(RobotPlayer.team.opponent())) {
			enemySeenAt(rc, location);
		}
	}
	
	public static Vector getNearbyKnownEnemyLocation(RobotController rc, Vector location) {
		int x = 8 * roundX(location.dx) / 8;
		int y = 8 * roundY(location.dy) / 8;
		
		for (int limit = 1; limit <= Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION; limit++) {
			int adjustment = 8 * (limit % 2 == 0 ? 1 : -1);
			for (int steps = limit; steps > -limit; steps--) {
				System.out.println(String.format("checking %d, %d", x, y));
				if (y < 0 || y >= Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION * 8) {
//					System.out.println(String.format("y: %d, minY: %d", y, minY));
				} else if (x < 0 || x >= Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION * 8) {
//					System.out.println(String.format("x: %d, minX: %d", x, minX));
					// jump ahead
				} else if (MapInfo.left > 0 && x + minX < MapInfo.left - 8
						|| MapInfo.right > 0 && x + minX > MapInfo.right + 8
						|| MapInfo.top > 0 && y + minY > MapInfo.top + 8
						|| MapInfo.bottom > 0 && y + minY < MapInfo.bottom - 8) {
					System.out.println(String.format("Map: left: %f, right: %f, bottom: %f, top: %f", MapInfo.left, MapInfo.right, MapInfo.bottom, MapInfo.top));
					// jump ahead
				} else {
					System.out.println(String.format("%d %d on the map: (%d, %d)", x, y, x + minX, y + minY));
					rc.setIndicatorDot(new MapLocation(x, y), 147, 147, 147);
					int page = getPage(x, y);
					int val = Messages.readInt(rc, Messages.KNOWN_ENEMY_LOCATIONS_START + page);
					if (val != 0 || (val = Messages.readInt(rc, Messages.KNOWN_ENEMY_LOCATIONS_START + ++page)) != 0) {
						System.out.println(String.format("page %d value %s", page, Integer.toHexString(val)));
						int locationOffset = BIT_POSITION_LOOKUP[-val & val % 37];
						return offsetToVector(locationOffset).add(pageToVector(page));
					}
				}
				if (steps > 0) {
					x += adjustment;
				} else {
					y += adjustment;
				}
			}
		}
		return location;
	}
	
	private static Vector offsetToVector(int bitOffset) {
		Vector vector = new Vector(bitOffset % 8, bitOffset / 8);
		System.out.println("bitoffset: " + bitOffset + " vector " + vector);
		return vector;
	}
	
	private static Vector pageToVector(int page) {
		return new Vector((page / 2) % Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION,
				(page / 2) / Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION)
				.scale(8)
				.add(minX, minY)
				.add(new Vector(0, page % 2 == 0 ? 0 : 4));
	}
	
	public static void enemySeenAt(RobotController rc, MapLocation location) {
		int x = roundX(location.x);
		int y = roundY(location.y);
		set(rc, x, y, true);
	}
	
	private static int roundY(float y) {
		return Math.round(y) - minY;
	}
	
	private static int roundX(float x) {
		return Math.round(x) - minX;
	}
	
	private static void set(RobotController rc, int x, int y, boolean newValue) {
		int page = getPage(x, y);
		int bitOffset = getBitOffset(x, y);
		if (bitOffset >= 32) {
			page++;
			bitOffset -= 32;
		}
		int val = Messages.readInt(rc, Messages.KNOWN_ENEMY_LOCATIONS_START + page);
		int mask = 1 << bitOffset;
		if (newValue) {
			val = val | mask;
		} else {
			val = val & (0xffffffff ^ mask);
		}
		System.out.println(String.format("writing %d %d on map, logical page %d, mask %s", x + minX, y + minY, page, Integer.toHexString(mask)));
		Messages.writeInt(rc, Messages.KNOWN_ENEMY_LOCATIONS_START + page, val);
	}
	
	private static int getPage(int x, int y) {
		return ((y / 8) * Messages.KNOWN_ENEMY_LOCATIONS_DIMENSION + (x / 8)) * 2;
	}
	
	private static int getBitOffset(int x, int y) {
		return (y % 8) * 8 + x % 8;
	}
	
	private KnownEnemyLocations() {
		// Don't instantiate
	}
	
}
