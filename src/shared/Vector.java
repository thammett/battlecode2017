package shared;

import battlecode.common.Direction;
import battlecode.common.MapLocation;


public final class Vector {
	
	public static final Vector NULL = new Vector(0, 0);
	
	public static Vector position(MapLocation location) {
		return new Vector(location.x, location.y);
	}
	
	public static Vector subtract(MapLocation l0, MapLocation l1) {
		return new Vector(l0.x - l1.x, l0.y - l1.y);
	}
	
	public static Vector subtract(MapLocation l0, Vector l1) {
		return new Vector(l0.x - l1.dx, l0.y - l1.dy);
	}
	
	public static Vector unit(double radians) {
		return polar(1, radians);
	}
	
	public static Vector polar(double r, double theta) {
		return new Vector(r * Math.cos(theta), r * Math.sin(theta));
	}
	
	public final float dx;
	public final float dy;
	
	public Vector(double dx, double dy) {
		this((float) dx, (float) dy);
	}
	
	public Vector(float dx, float dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public Vector add(MapLocation location) {
		return new Vector(dx + location.x, dy + location.y);
	}
	
	public Vector add(Vector v) {
		return new Vector(dx + v.dx, dy + v.dy);
	}
	
	public Vector add(float dx0, float dy0) {
		return new Vector(dx + dx0, dy + dy0);
	}
	
	public Vector subtract(MapLocation location) {
		return new Vector(dx - location.x, dy - location.y);
	}
	
	public Vector subtract(Vector other) {
		return new Vector(dx - other.dx, dy - other.dy);
	}
	
	public Vector normalize() {
		float magnitude = magnitude();
		return magnitude == 0 ? this : scale(1 / magnitude);
	}
	
	public float magnitude() {
		return (float) Math.sqrt(dx * dx + dy * dy);
	}
	
	public Vector scale(float scalar) {
		return new Vector(dx * scalar, dy * scalar);
	}
	
	public Vector truncate(float limit) {
		float magnitude = magnitude();
		return magnitude == 0 || magnitude < limit ? this : scale(limit / magnitude);
	}
	
	public Vector rotate(float radians) {
		return unit(angle() + radians).scale(magnitude());
	}
	
	public float angle() {
		return (float) Math.atan2(dy, dx);
	}
	
	public boolean rightOf(Vector other) {
		return dx * -other.dy + dy * other.dx < 0;
	}
	
	public Direction toDirection() {
		return dx == 0 && dy == 0 ? null : new Direction(dx, dy);
	}
	
	public MapLocation toLocation() {
		return new MapLocation(dx, dy);
	}
	
	@Override
	public String toString() {
		return "Vector: " + dx + "," + dy;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj == this || (obj instanceof Vector) && ((Vector) obj).dx == dx && ((Vector) obj).dy == dy;
	}
	
	@Override
	public int hashCode() {
		return 37 * Float.floatToIntBits(dx) + Float.floatToIntBits(dy);
	}
	
}
