package shared;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public final class BuildOrder {
	
	public static RobotType[] initialBuild = {RobotType.SCOUT, RobotType.LUMBERJACK};
	public static boolean finished;
	
	public static boolean finished(RobotController rc) {
		return finished || (finished = Messages.readBuildCount(rc) >= initialBuild.length);
	}
	
	public static RobotType getNextType(RobotController rc) {
		return initialBuild[Messages.readBuildCount(rc)];
	}
	
	public static void increment(RobotController rc) {
		int count = Messages.readBuildCount(rc) + 1;
		Messages.writeBuildCount(rc, count);
		if (count == initialBuild.length) {
			finished = true;
		}
	}
	
}
