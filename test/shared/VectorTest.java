package shared;

import static org.junit.Assert.*;

import org.junit.Test;


public final class VectorTest {
	
	public static final float EPSILON = 0.0000001f;
	
	@Test
	public void testAngle() {
		assertEquals(0, new Vector(10, 0).angle(), EPSILON);
		assertEquals(Math.PI, new Vector(-10, 0).angle(), EPSILON);
		assertEquals(Math.PI / 2, new Vector(0, 10).angle(), EPSILON);
		assertEquals(Math.PI / -2, new Vector(0, -10).angle(), EPSILON);
		assertEquals(Math.PI / 4, new Vector(10, 10).angle(), EPSILON);
	}
	
}
